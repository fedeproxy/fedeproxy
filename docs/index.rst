Fedeproxy
=========

`Fedeproxy <https://fedeproxy.eu/>`__ is an online service to federate forges. The software projects hosted on one forge are synchronized in real time with their counterparts on other forges, via the W3C ActivityPub protocol. Developers can freely use the forge of their choosing while contributing to the same project. It operates independently from the forges and serves as an incubator with rapid prototyping to research the best user experience.

The project is in its experimental development stage and you can follow its progress `on the forge where it is being worked on <https://lab.fedeproxy.eu/fedeproxy/fedeproxy/-/milestones>`__ or by reading the `monthly updates <https://fedeproxy.eu/blog/>`__.

.. toctree::
  :caption: Fedeproxy
  :name: Fedeproxy
  :maxdepth: 2

  hacking
  release-notes

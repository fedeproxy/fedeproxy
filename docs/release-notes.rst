Release Notes
=============

0.0.3
-----

* Implement `nodeinfo protocol <https://nodeinfo.diaspora.software/>`__

0.0.2
-----

* Implement loading and saving issues to files
* Define a format for issues, with json-schema validation and documentation
* Add support for Mercurial in addition to Git
* Implement project export in the fedeproxy domain
* Define the fedeproxy domain
* Reorganize the GitLab and Gitea clases to separate Issues and Projects
* Allow actions to be performed by non-privileged users
* User representation and implementation for Gitea and GitLab
* GitLab export feature

0.0.1
-----

* Setup Gitea and GitLab instances in the CI
* Integration tests
* Interfaces and concrete implementation for Gitea, GitLab and git as a DVCS
* Organize the codebase and separate the domain from the architecture


from django.apps import AppConfig


class FedeproxyAuthAppConfig(AppConfig):
    label = "fedeproxy_auth"
    name = "fedeproxy.auth"
    verbose_name = "Fedeproxy Auth"
